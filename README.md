# Spok 
Spok is a secure port knocking server using TOTP as a part of its
sequence generation.

# Server
The server is written in Java. It required super user right to run.

# Java client
The Java client is deprecated and not developed any more.
It can only do TCP knocking, but doesn't require super user right to run.

# Python client
The Python client is actively developed and can do more sophisticated 
type of knocking. It requires super user to run.

# Testing port knocking
3 Virtualbox machine running Ubuntu 14.04
* Machine "Firewall" with 2 interface 192.168.13.100 & 192.168.14.100
* Machine "SSH" with 1 interface 192.168.14.101
* Machine "User" with 1 interface 192.168.13.101

1. Run port knocking client on "User" to make "Firewall" open a port.
2. Run port knocking client on "User" to make "Firewall" forward a port to "SSH"