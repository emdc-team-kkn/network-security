package tecnico.kkn.network;

import org.pcap4j.core.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.spok.KnockAttempt;
import tecnico.kkn.spok.KnockConfig;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class handle package snooping and capturing
 */
public class NetworkInterfaceListener {
    private final static Logger log = LoggerFactory.getLogger(NetworkInterfaceListener.class);

    private final PcapNetworkInterface pnif;
    private final int readTimeout;
    private final int snapLen;
    private final ExecutorService executorService;
    private final Map<String, KnockAttempt> ipAttemptMap;
    private final Set<KnockConfig> knockConfigs;

    /**
     * Default filter
     * We only capture IPv4 TCP packet
     */
    private final static String DEFAULT_FILTER = "ip proto \\tcp";

    public NetworkInterfaceListener(PcapNetworkInterface pnif, int readTimeout, int snapLen, Set<KnockConfig> knockConfigs) {
        this.pnif = pnif;
        this.readTimeout = readTimeout;
        this.snapLen = snapLen;
        this.knockConfigs = knockConfigs;
        this.executorService = Executors.newCachedThreadPool();
        this.ipAttemptMap = new ConcurrentHashMap<>();
    }

    public void run() {
        try {
            final PcapHandle handle = pnif.openLive(
                    snapLen,
                    PcapNetworkInterface.PromiscuousMode.NONPROMISCUOUS,
                    readTimeout
            );

            PacketListener listener = new PacketListenerImpl(handle, ipAttemptMap, knockConfigs);

            // TODO: Generate a better filter based on our knockConfigs
            handle.setFilter(DEFAULT_FILTER, BpfProgram.BpfCompileMode.OPTIMIZE);

            // Loop to get the packet
            handle.loop(-1, listener, executorService);

            executorService.shutdown();

            PcapStat ps = handle.getStats();
            System.out.println("ps_recv: " + ps.getNumPacketsReceived());
            System.out.println("ps_drop: " + ps.getNumPacketsDropped());
            System.out.println("ps_ifdrop: " + ps.getNumPacketsDroppedByIf());

            handle.close();
        } catch (InterruptedException e) {
            log.error("Unhandled exception", e);
        } catch (PcapNativeException e) {
            log.error("Unhandled exception", e);
        } catch (NotOpenException e) {
            log.error("Unhandled exception", e);
        }
    }
}
