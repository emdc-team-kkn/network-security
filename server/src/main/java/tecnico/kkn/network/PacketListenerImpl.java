package tecnico.kkn.network;

import org.pcap4j.core.PacketListener;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.packet.IpV4Packet;
import org.pcap4j.packet.Packet;
import org.pcap4j.packet.TcpPacket;
import org.pcap4j.packet.namednumber.IpNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.spok.KnockAttempt;
import tecnico.kkn.spok.KnockConfig;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * This class handle the interaction when we capture a packet.
 */
public class PacketListenerImpl implements PacketListener {
    private final Logger log = LoggerFactory.getLogger(PacketListenerImpl.class);

    private final PcapHandle handle;
    private final Map<String, KnockAttempt> ipAttemptMap;
    private final Set<KnockConfig> knockConfigs;
    private final ScheduledExecutorService executorService;
    private static final long ATTEMPT_TIMEOUT  = 30;

    public PacketListenerImpl(PcapHandle handle, Map<String, KnockAttempt> ipAttemptMap, Set<KnockConfig> knockConfigs) {
        this.handle = handle;
        this.ipAttemptMap = ipAttemptMap;
        this.knockConfigs = knockConfigs;
        this.executorService = Executors.newScheduledThreadPool(10);
    }

    @Override
    public void gotPacket(Packet packet) {
        IpV4Packet ipv4Packet = packet.get(IpV4Packet.class);
        if (ipv4Packet == null) {
            log.debug("Invalid IPv4 packet. Ignore");
            return;
        }

        IpV4Packet.IpV4Header header = ipv4Packet.getHeader();
        log.debug("Source address: {}", header.getSrcAddr());
        if (!header.getProtocol().equals(IpNumber.TCP)) {
            log.debug("Unsupported packet type. Only TCP are supported");
            return;
        }

        TcpPacket tcpPacket = ipv4Packet.get(TcpPacket.class);
        TcpPacket.TcpHeader tcpHeader = tcpPacket.getHeader();
        int destinationPort = tcpHeader.getDstPort().valueAsInt();

        Packet payloadPacket = tcpPacket.getPayload();
        String payload = null;
        if (payloadPacket != null) {
            byte[] rawPayload = payloadPacket.getRawData();
            payload = new String(rawPayload);

        }
        log.debug("Payload: {}", payload);
        log.debug("Destination port: {}", destinationPort);
        final String sourceAddress = header.getSrcAddr().getHostAddress();
        boolean finalMatch;
        if (ipAttemptMap.containsKey(sourceAddress)) {
            KnockAttempt ka = ipAttemptMap.get(sourceAddress);
            finalMatch = ka.match(destinationPort, payload);
            if (finalMatch) {
                // If there are no match or the knock is finished, remove the KnockAttempt
                log.info("Attempt complete. Removing");
                ipAttemptMap.remove(sourceAddress);
            }
        } else {
            Map<KnockConfig, Integer> stageMap = new HashMap<>();
            for(KnockConfig kc: knockConfigs) {
                stageMap.put(kc, 0);
            }
            final KnockAttempt ka = new KnockAttempt(
                    stageMap,
                    header.getSrcAddr(),
                    new Timestamp(handle.getTimestampInts() * 1000)
            );
            // Try to match
            finalMatch = ka.match(destinationPort, payload);
            if(!finalMatch) {
                log.info("New attempt detected");
                // If it's not the final match, put the match in the stagemap.
                ipAttemptMap.put(sourceAddress, ka);
                // Schedule this to be removed from map after timeout.
                this.executorService.schedule(new Runnable() {
                    @Override
                    public void run() {
                        log.info("Checking for removal");
                        if (ka == ipAttemptMap.get(sourceAddress)) {
                            // Only remove if this is the attempt we put in
                            log.info("Timeout exceed. Removing attempt.");
                            ipAttemptMap.remove(sourceAddress);
                        }
                    }
                }, ATTEMPT_TIMEOUT, TimeUnit.SECONDS);
            }
        }

        log.debug("Receive from {}, stage {}", sourceAddress, ipAttemptMap.get(sourceAddress));
    }
}
