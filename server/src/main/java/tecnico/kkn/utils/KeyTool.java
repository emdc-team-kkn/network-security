package tecnico.kkn.utils;

import org.apache.commons.codec.binary.Base32;
import tecnico.kkn.exception.InvalidKeyException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * This class handle key generation & manipulation
 */
public class KeyTool {

    private final static int MIN_KEY_SIZE = 20;
    private final Random random = new SecureRandom();
    private final static String SMALL_KEY_MSG =
            String.format("Your key size is too small, it must be at least %d bytes.", MIN_KEY_SIZE);
    private final static String INVALID_CHR_MSG =
            String.format("Your key contain invalid character");

    /**
     * Generate the key in byte with the specify key size.
     * The output is the base32 encoded string of the key.
     * This is so that the key can be used with google authenticator.
     * @param keysize The keysize to generate, must be larger than 20
     * @return The base32 encoded key
     * @throws InvalidKeyException
     */
    public String genKey(int keysize) throws InvalidKeyException {
        if (keysize < MIN_KEY_SIZE) {

            throw new InvalidKeyException(SMALL_KEY_MSG);
        }

        // Generate a random key
        byte[] out = new byte[keysize];
        random.nextBytes(out);

        Base32 base32 = new Base32();
        return base32.encodeAsString(out);
    }

    /**
     * Derive a secondary key from the secret key to use for packet checksum.
     * @param masterKey The input master key
     * @return The derived key
     */
    public String deriveKey(String masterKey) throws NoSuchAlgorithmException {
        Base32 base32 = new Base32();
        byte[] decodedKey = base32.decode(masterKey);

        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(decodedKey);

        byte[] derivedKey = md.digest();

        return base32.encodeAsString(derivedKey);
    }

    /**
     * Validate if a key supplied is correctly encoded and is of sufficient length.
     * @param key The key to be checked
     * @throws InvalidKeyException
     */
    public void validateKey(String key) throws InvalidKeyException {
        Base32 base32 = new Base32();
        byte[] decodedKey = base32.decode(key);

        if (!base32.isInAlphabet(key)) {
            throw new InvalidKeyException(KeyTool.INVALID_CHR_MSG);
        }

        if (decodedKey.length < MIN_KEY_SIZE) {
            throw new InvalidKeyException(KeyTool.SMALL_KEY_MSG);
        }
    }
}
