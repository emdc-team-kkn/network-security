package tecnico.kkn.exception;

/**
 * This exception is thrown when we detect an error in the configuration.
 */
public class InvalidConfigurationException extends Exception {
    public InvalidConfigurationException(String s) {
        super(s);
    }

    public InvalidConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
