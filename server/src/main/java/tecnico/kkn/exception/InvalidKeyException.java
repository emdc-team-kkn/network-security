package tecnico.kkn.exception;

/**
 * This exception is used when the key is invalid
 */
public class InvalidKeyException extends Exception {

    public InvalidKeyException(String s) {
        super(s);
    }

}
