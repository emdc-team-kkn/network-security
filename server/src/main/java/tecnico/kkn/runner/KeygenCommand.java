package tecnico.kkn.runner;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.exception.InvalidKeyException;
import tecnico.kkn.utils.KeyTool;

import java.util.ArrayList;
import java.util.List;

/**
 * This class handle in keygen subcommand
 */
public class KeygenCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(KeygenCommand.class);

    @Option(name = "-s",
            usage = "The size of the key in bytes to be generated. Default: 20 bytes",
            metaVar = "<the length of the key>")
    private int keysize = 20;

    // receives other command line parameters than options
    @Argument
    private List<String> arguments = new ArrayList<String>();

    @Override
    public void execute() {
        KeyTool keyTool = new KeyTool();
        try {
            String key = keyTool.genKey(keysize);
            System.out.println(key);
        } catch (InvalidKeyException e) {
            System.err.println(e.getMessage());
        }
    }
}
