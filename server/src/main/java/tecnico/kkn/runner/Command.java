package tecnico.kkn.runner;

/**
* Created by dikei on 11/27/14.
*/
public interface Command {
    public void execute();
}
