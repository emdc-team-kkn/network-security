package tecnico.kkn.runner;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.Pcaps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.exception.InvalidConfigurationException;
import tecnico.kkn.network.NetworkInterfaceListener;
import tecnico.kkn.spok.KnockConfig;
import tecnico.kkn.spok.KnockConfigFactory;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
* This class handle server subcommand
*/
public class ServerCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(ServerCommand.class);

    @Option(name = "-i",
            usage = "The interface to listen on",
            metaVar = "<network interface>",
            required = true)
    private String interfaceName;

    @Option(name = "-c",
            usage = "The configuration file",
            metaVar = "<config file path>",
            required = true)
    private String configurationFile;

    // receives other command line parameters than options
    @Argument
    private List<String> arguments = new ArrayList<String>();

    @Override
    public void execute() {
        try {
            // Parse configuration file and generate a set of KnockConfigs
            KnockConfigFactory knockConfigFactory = new KnockConfigFactory();
            Set<KnockConfig> knockConfigs = knockConfigFactory.readKnockConfigs(configurationFile);

            PcapNetworkInterface nif = Pcaps.getDevByName(interfaceName);
            // No interface selected
            if (nif == null) {
                throw new IOException();
            }

            NetworkInterfaceListener listener = new NetworkInterfaceListener(nif, 30, 65536, knockConfigs);
            listener.run();
        } catch (IOException | PcapNativeException e) {
            log.error("Unable to open network interface", e);
            System.exit(-1);
        } catch (NoSuchAlgorithmException e) {
            log.error("Unsupported hash algorithm. Update your Java", e);
            System.exit(-1);
        } catch (InvalidConfigurationException e) {
            System.out.println("Your configuration file is invalid");
            System.out.println(e.getMessage());
            if (e.getCause() != null) {
                System.out.println(e.getCause().getMessage());
            }
        }
    }
}
