package tecnico.kkn.runner;

import org.kohsuke.args4j.*;
import org.kohsuke.args4j.spi.SubCommand;
import org.kohsuke.args4j.spi.SubCommandHandler;
import org.kohsuke.args4j.spi.SubCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the runner class of the program, hosting the main method.
 */
public class Runner {
    private static final Logger log = LoggerFactory.getLogger(Runner.class);

    @Argument(
            usage = "Sub command of our server binary",
            handler = SubCommandHandler.class,
            metaVar = "<subcommand>",
            required = true
    )
    @SubCommands({
        @SubCommand(name = "keygen", impl = KeygenCommand.class),
        @SubCommand(name = "server", impl = ServerCommand.class)
    })
    private Command cmd;

    public void run(String[] args) {
        ParserProperties parserProperties = ParserProperties.defaults();
        parserProperties.withUsageWidth(80);
        CmdLineParser parser = new CmdLineParser(this, parserProperties);

        try {
            parser.parseArgument(args);
            cmd.execute();
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java tecnico.kkn.runner.Runner [subcommand] [options...] arguments...");
            parser.printUsage(System.err);
            parser.printExample(OptionHandlerFilter.ALL);
            CmdLineParser subCommandParser = e.getParser();
            if (!subCommandParser.equals(parser)) {
                subCommandParser.printUsage(System.err);
                subCommandParser.printExample(OptionHandlerFilter.ALL);
            }
            System.err.println();
            System.exit(-1);
        }
    }

    public static void main(String[] args) {
        new Runner().run(args);
    }

}
