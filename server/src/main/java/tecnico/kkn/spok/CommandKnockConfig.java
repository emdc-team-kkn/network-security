package tecnico.kkn.spok;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

/**
 * This knock configuration run a command when execute
 */
public class CommandKnockConfig extends KnockConfig {
    private String command;
    private String stopCommand;

    public void setCommand(String command) {
        this.command = command;
    }

    public void setStopCommand(String stopCommand) {
        this.stopCommand = stopCommand;
    }

    /**
     * Execute this configuration against the source address
     * @param source The source address of the knock attemp
     */
    public void execute(InetAddress source) {
        super.execute(source);
        IptablesExecutor iptablesExecutor = new IptablesExecutor(source, command);
        iptablesExecutor.run();
        if (sessionTimeout > 0) {
            this.executorService.schedule(
                    new IptablesExecutor(source, stopCommand),
                    sessionTimeout,
                    TimeUnit.SECONDS
            );
        }
    }
}
