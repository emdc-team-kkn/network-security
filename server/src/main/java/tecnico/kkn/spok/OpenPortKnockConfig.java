package tecnico.kkn.spok;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

/**
 * This type of knock configuration open a port when execute
 */
public class OpenPortKnockConfig extends KnockConfig {

    private short port;
    private String openProtocol = "tcp";

    private static final String OPEN_COMMAND =
            "iptables -I INPUT -s %IP% -p %PROTOCOL% --dport %PORT% -j ACCEPT";

    private static final String CLOSE_COMMAND =
            "iptables -D INPUT -s %IP% -p %PROTOCOL% --dport %PORT% -j ACCEPT";

    public void setPort(short port) {
        this.port = port;
    }

    public void setOpenProtocol(String openProtocol) {
        this.openProtocol = openProtocol;
    }

    public void execute(InetAddress source) {
        super.execute(source);
        String command = OPEN_COMMAND
                .replace("%PORT%", String.valueOf(port))
                .replace("%PROTOCOL%", openProtocol);
        String stopCommand = CLOSE_COMMAND
                .replace("%PORT%", String.valueOf(port))
                .replace("%PROTOCOL%", openProtocol);

        IptablesExecutor iptablesExecutor = new IptablesExecutor(source, command);
        iptablesExecutor.run();
        if (sessionTimeout > 0) {
            this.executorService.schedule(
                    new IptablesExecutor(source, stopCommand),
                    sessionTimeout,
                    TimeUnit.SECONDS
            );
        }
    }
}
