package tecnico.kkn.spok;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

/**
 * Created by dikei on 12/3/14.
 */
public class ForwardPortKnockConfig extends KnockConfig {

    /**
     * iptables -t nat -A PREROUTING -p tcp --dport %FPORT% -j DNAT --to-destination %DEST%:%DPORT%
     * iptables -A FORWARD -m conntrack -p tcp -d %DEST% --dport %DPORT% --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
     * iptables -A FORWARD -m conntrack -p tcp -s %DEST% --sport %DPORT% --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
     */
    private short port;
    private String dhost;
    private short dport;
    private String forwardProtocol = "tcp";

    private final static String NAT_COMMAND =
            "iptables -t nat -I PREROUTING -s %IP% -p %PROTOCOL% --dport %PORT% " +
                    "-j DNAT --to-destination %DHOST%:%DPORT%";
    private final static String FORWARD_COMMAND1 =
            "iptables -I FORWARD -p tcp -s %IP% -d %DHOST% --dport %DPORT% " +
                    "-m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT";
    private final static String FORWARD_COMMAND2 =
            "iptables -I FORWARD -p tcp -d %IP% -s %DHOST% --sport %DPORT% " +
                    "-m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT";

    private final static String STOP_NAT_COMMAND =
            "iptables -t nat -D PREROUTING -s %IP% -p %PROTOCOL% --dport %PORT% " +
                    "-j DNAT --to-destination %DHOST%:%DPORT%";
    private final static String STOP_FORWARD_COMMAND1 =
            "iptables -D FORWARD -p tcp -s %IP% -d %DHOST% --dport %DPORT% " +
                    "-m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT";
    private final static String STOP_FORWARD_COMMAND2 =
            "iptables -D FORWARD -p tcp -d %IP% -s %DHOST% --sport %DPORT% " +
                    "-m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT";

    public void setPort(Short port) {
        this.port = port;
    }

    public void setDhost(String dhost) {
        this.dhost = dhost;
    }

    public void setDport(short dport) {
        this.dport = dport;
    }

    public void setForwardProtocol(String forwardProtocol) {
        this.forwardProtocol = forwardProtocol;
    }

    @Override
    public void execute(InetAddress source) {
        super.execute(source);
        String natCmd = NAT_COMMAND
                .replace("%PORT%", String.valueOf(port))
                .replace("%DHOST%", dhost)
                .replace("%DPORT%", String.valueOf(dport))
                .replace("%PROTOCOL%", forwardProtocol);
        String forwardCmd1 = FORWARD_COMMAND1
                .replace("%DHOST%", dhost)
                .replace("%DPORT%", String.valueOf(dport))
                .replace("%PROTOCOL%", forwardProtocol);
        String forwardCmd2 = FORWARD_COMMAND2
                .replace("%DHOST%", dhost)
                .replace("%DPORT%", String.valueOf(dport))
                .replace("%PROTOCOL%", forwardProtocol);

        this.executorService.submit(new IptablesExecutor(source, natCmd));
        this.executorService.submit(new IptablesExecutor(source, forwardCmd1));
        this.executorService.submit(new IptablesExecutor(source, forwardCmd2));
        if (sessionTimeout > 0) {
            String stopNatCmd = STOP_NAT_COMMAND
                    .replace("%PORT%", String.valueOf(port))
                    .replace("%DHOST%", dhost)
                    .replace("%DPORT%", String.valueOf(dport))
                    .replace("%PROTOCOL%", forwardProtocol);
            String stopForwardCmd1 = STOP_FORWARD_COMMAND1
                    .replace("%DHOST%", dhost)
                    .replace("%DPORT%", String.valueOf(dport))
                    .replace("%PROTOCOL%", forwardProtocol);
            String stopForwardCmd2 = STOP_FORWARD_COMMAND2
                    .replace("%DHOST%", dhost)
                    .replace("%DPORT%", String.valueOf(dport))
                    .replace("%PROTOCOL%", forwardProtocol);
            this.executorService.schedule(
                    new IptablesExecutor(source, stopNatCmd),
                    sessionTimeout,
                    TimeUnit.SECONDS
            );
            this.executorService.schedule(
                    new IptablesExecutor(source, stopForwardCmd1),
                    sessionTimeout,
                    TimeUnit.SECONDS
            );
            this.executorService.schedule(
                    new IptablesExecutor(source, stopForwardCmd2),
                    sessionTimeout,
                    TimeUnit.SECONDS
            );
        }
    }
}
