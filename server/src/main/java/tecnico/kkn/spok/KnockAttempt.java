package tecnico.kkn.spok;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.exception.MismatchPayloadException;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This is used to stored knock attempt by an IP
 */
public class KnockAttempt {
    private final static Logger log = LoggerFactory.getLogger(KnockAttempt.class);

    private Inet4Address source;
    private Map<KnockConfig, Integer> stageMap;
    private Timestamp startTime;
    private long startTimecode;

    public KnockAttempt(Map<KnockConfig, Integer> stageMap, Inet4Address source, Timestamp startTime) {
        this.stageMap = stageMap;
        this.source = source;
        this.startTime = startTime;
        this.startTimecode = this.startTime.getTime() / 1000 / 30;
    }

    /**
     * Do the port matching and execute the command if necessary.
     * @param port The port to matched our KnockConfig against.
     * @return true if this is the final matched, else false
     */
    public boolean match(int port, String payload) {
        // TODO: Add more check for validity of packets.
        Set<KnockConfig> invalidConfig = new HashSet<>();
        boolean finalMatch = false;
        for(KnockConfig config: stageMap.keySet()) {
            int stage = stageMap.get(config);
            try {
                boolean matched = config.match(port, stage, payload, startTimecode);
                if (matched) {
                    // Every thing match, increase the stage of this config
                    stage = stage + 1;
                    stageMap.put(config, stage);

                    // A config has reach final stage. Execute the command.
                    if (stage == config.getFinalStage()) {
                        config.execute(source);
                        finalMatch = true;
                        break;
                    }
                } else {
                    // Remove the config
                    invalidConfig.add(config);
                }
            } catch (MismatchPayloadException e) {
                // We ignore the packet & add the config if the payload is mismatched
                // Unless it's in stage 0, then we ignore the attempt
                if (stage == 0) {
                    invalidConfig.add(config);
                }
            }
        }
        // Remove invalid configuration
        for(KnockConfig k: invalidConfig) {
            stageMap.remove(k);
        }
        // If the stageMap is empty, this is the final match
        if (stageMap.isEmpty()) {
            finalMatch = true;
        }
        return finalMatch;
    }

    @Override
    public String toString() {
        return "KnockAttempt{" +
                "source=" + source +
                ", startTime=" + startTime +
                '}';
    }
}