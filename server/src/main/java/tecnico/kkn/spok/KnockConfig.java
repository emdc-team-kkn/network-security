package tecnico.kkn.spok;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.exception.MismatchPayloadException;
import tecnico.kkn.utils.TOTP;

import java.net.InetAddress;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * This class store a knocking configuration
 */
public abstract class KnockConfig {
    private final static Logger log = LoggerFactory.getLogger(KnockConfig.class);

    private List<Short> sequence;
    private String name;
    private String key;
    private String secondaryKey;
    private int protocol;
    private int sequenceSize;
    private boolean disabled;
    private final static int GRACE_PERIOD = 60;
    protected long sessionTimeout;
    protected ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);

    /**
     * Set the common variable for a configuration
     * @param name
     * @param protocol
     * @param key
     * @param derivedKey
     * @param sequenceSize
     * @param configSequence
     * @param sessionTimeout
     */
    public void setConfig(String name, int protocol, String key, String derivedKey, int sequenceSize, List<Short> configSequence, int sessionTimeout) {
        this.name = name;
        this.protocol = protocol;
        this.key = key;
        this.secondaryKey = derivedKey;
        this.sequenceSize = sequenceSize;
        this.sequence = configSequence;
        this.sessionTimeout = sessionTimeout;
    }

    /**
     * Return the final stage of the knock config.
     * @return the final stage
     */
    public int getFinalStage() {
        if (protocol == 1) {
            return sequenceSize + 1;
        } else {
            return sequenceSize;
        }
    }

    /**
     * Execute the action associated with this configuration against the source address
     * @param source The source of the knock
     */
    public void execute(InetAddress source) {
        log.debug("Disable config for {} seconds", GRACE_PERIOD);
        this.disabled = true;
        this.executorService.schedule(new Runnable() {
            @Override
            public void run() {
                log.debug("Re-enable knock configuration.");
                disabled = false;
            }
        }, GRACE_PERIOD, TimeUnit.SECONDS);
    }

    /**
     * Match this configuration against a knock attempt
     * @param port The port of the packet
     * @param stage The stage of the attempt
     * @param payload The payload of the packet
     * @param startTimeCode The start timecode of this attempt
     * @return true if matched, false otherwise
     * @throws MismatchPayloadException
     */
    public boolean match(int port, int stage, String payload, long startTimeCode)
            throws MismatchPayloadException {
        boolean verify = verifyPayload(payload, stage, startTimeCode);
        if (!verify) {
            throw new MismatchPayloadException();
        }
        if (this.disabled) {
            return false;
        }
        if (this.protocol == 1) {
            return matchProtocol1(port, stage);
        } else if (this.protocol == 2) {
            return matchProtocol2(port, stage, startTimeCode);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "KnockConfig{" +
                "sequence=" + sequence +
                ", name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", protocol=" + protocol +
                ", sequenceSize=" + sequenceSize +
                '}';
    }

    /**
     * Verify the payload to match
     * @param payload The payload to verify against
     * @param startTimeCode The start time code of the attemp
     * @return true if the verify is success
     */
    private boolean verifyPayload(String payload, int stage, long startTimeCode) {
        Base32 base32 = new Base32();
        byte[] byteKey = base32.decode(secondaryKey);

        // Encode the key into hex
        // because our TOTP function use hex-encoded string as input
        String hexKey = Hex.encodeHexString(byteKey).toUpperCase();

        // Generate the TOTP code
        String steps = Long.toHexString(stage + startTimeCode).toUpperCase();
        while (steps.length() < 16) steps = "0" + steps;

        String totp = TOTP.generateTOTP(hexKey, steps, "6");
        return totp.equals(payload);
    }

    /**
     * Match against protocol 1 knock configuration
     * @param port The port of the packet
     * @param stage The stage of the attempt
     * @return true if matched, false otherwise
     */
    private boolean matchProtocol1(int port, int stage) {
        if (stage < sequence.size()) {
            return port == sequence.get(stage);
        } else if (stage == sequence.size()) {
            long timecode = System.currentTimeMillis() / 1000 / 30;
            int totpPort = genTotpPort(key, timecode);
            return port == totpPort;
        } else {
            return false;
        }
    }

    /**
     * Match against protocol 2 knock configuration
     * @param port The port of the packet
     * @param stage The stage of the attempt
     * @param startTimecode The start timecode of the attemp
     * @return true if matched, false otherwise
     */
    private boolean matchProtocol2(int port, int stage, long startTimecode) {
        int totpPort = genTotpPort(key, startTimecode + stage);
        return port == totpPort;
    }

    /**
     * Generate the totp port for the specified key and timecode
     * @param key The key
     * @param timecode The timecode
     * @return The port
     */
    private int genTotpPort(String key, long timecode) {
        // Decode the key into byte
        Base32 base32 = new Base32();
        byte[] byteKey = base32.decode(key);

        // Encode the key into hex
        // because our TOTP function use hex-encoded string as input
        String hexKey = Hex.encodeHexString(byteKey).toUpperCase();

        // Generate the TOTP code
        String steps = Long.toHexString(timecode).toUpperCase();
        while (steps.length() < 16) steps = "0" + steps;

        String totp = TOTP.generateTOTP(hexKey, steps, "6");
        return Integer.valueOf(totp) % 65536;
    }

}
