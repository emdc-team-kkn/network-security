package tecnico.kkn.spok;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;

/**
 * This class execute the specified command with the source address.
 */
public class IptablesExecutor implements Runnable {
    private final static Logger log = LoggerFactory.getLogger(IptablesExecutor.class);

    private final InetAddress source;
    private final String command;

    public IptablesExecutor(InetAddress source, String command) {
        this.source = source;
        this.command = command;
    }

    @Override
    public void run() {
        String tobeExecute = command.replace("%IP%", source.getHostAddress());
        log.debug("Executing: {}", tobeExecute);
        try {
            Runtime.getRuntime().exec(tobeExecute);
        } catch (IOException | RuntimeException e) {
            log.error("Error running command", e);
        }
    }
}
