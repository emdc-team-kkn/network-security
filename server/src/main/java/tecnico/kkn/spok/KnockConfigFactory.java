package tecnico.kkn.spok;

import org.ini4j.Ini;
import org.ini4j.Profile;
import tecnico.kkn.exception.InvalidConfigurationException;
import tecnico.kkn.exception.InvalidKeyException;
import tecnico.kkn.utils.KeyTool;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * This class provide utilities to read configuration file and generate KnockConfig
 */
public class KnockConfigFactory {

    /**
     * Read the configuration file and return a set of KnockConfig
     * @param configFile The path to the configuration file.
     * @return The set of knock configuration
     * @throws IOException
     */
    public Set<KnockConfig> readKnockConfigs(String configFile) throws IOException, NoSuchAlgorithmException, InvalidConfigurationException {
        Set<KnockConfig> knockConfigs = new HashSet<>();
        Ini iniFile = new Ini(new FileInputStream(configFile));
        KeyTool keyTool = new KeyTool();

        for(String name: iniFile.keySet()) {
            Profile.Section section = iniFile.get(name);
            String type = section.get("type").toLowerCase();

            String key, derivedKey;
            int protocol;
            int sequenceSize;
            List<Short> configSequence = new ArrayList<>();
            int sessionTimeout;
            // Read protocol
            if (section.containsKey("protocol")) {
                protocol = Integer.parseInt(section.get("protocol"));
            } else {
                protocol = 1;
            }
            // Read sequence and sequence size
            if (protocol == 2) {
                sequenceSize = Integer.parseInt(section.get("sequence_size"));
            } else {
                String[] sequence = section.get("sequence").split(",");

                for(String port: sequence) {
                    try {
                        configSequence.add(Short.valueOf(port));
                    } catch (NumberFormatException e) {
                        String message = String.format("Section %s has an invalid port %s", name, port);
                        throw new InvalidConfigurationException(message);
                    }
                }
                sequenceSize = sequence.length;
            }
            // Read key
            if (section.containsKey("key")) {
                key = section.get("key").toUpperCase();
                try {
                    keyTool.validateKey(key);
                } catch (InvalidKeyException e) {
                    String message = String.format("Section %s has invalid configuration", name);
                    throw new InvalidConfigurationException(message, e);
                }
                derivedKey = keyTool.deriveKey(key);

            } else {
                String message = String.format("Section %s doesn't have a valid key", name);
                throw new InvalidConfigurationException(message);
            }
            // Read session timeout
            if (section.containsKey("session_timeout")) {
                sessionTimeout = Integer.parseInt(section.get("session_timeout"));
            } else {
                sessionTimeout = -1;
            }

            if (type.equals("command")) {
                CommandKnockConfig config = new CommandKnockConfig();
                if (section.containsKey("command")) {
                    config.setCommand(section.get("command"));
                } else if (section.containsKey("start_command")) {
                    config.setCommand(section.get("start_command"));
                } else {
                    String message = String.format("Section %s doesn't have a command or start_command", name);
                    throw new InvalidConfigurationException(message);
                }
                if (section.containsKey("stop_command")) {
                    if (sessionTimeout == -1) {
                        String message = String.format("Section %s doesn't have a valid session_timeout", name);
                        throw new InvalidConfigurationException(message);
                    } else {
                        config.setStopCommand(section.get("stop_command"));
                    }
                }
                config.setConfig(name, protocol, key, derivedKey, sequenceSize, configSequence, sessionTimeout);
                knockConfigs.add(config);
            } else if (type.equals("open")) {
                OpenPortKnockConfig config = new OpenPortKnockConfig();
                config.setPort(Short.valueOf(section.get("port")));
                if(section.containsKey("open_protocol")) {
                    config.setOpenProtocol(section.get("open_protocol"));
                }
                config.setConfig(name, protocol, key, derivedKey, sequenceSize, configSequence, sessionTimeout);
                knockConfigs.add(config);
            } else if (type.equals("forward")) {
                ForwardPortKnockConfig config = new ForwardPortKnockConfig();
                config.setPort(Short.valueOf(section.get("port")));
                config.setDport(Short.valueOf(section.get("dport")));
                config.setDhost(section.get("dhost"));
                if (section.containsKey("forward_protocol")) {
                    config.setForwardProtocol(section.get("forward_protocol"));
                }
                config.setConfig(name, protocol, key, derivedKey, sequenceSize, configSequence, sessionTimeout);
                knockConfigs.add(config);
            }
        }
        return knockConfigs;
    }
}
