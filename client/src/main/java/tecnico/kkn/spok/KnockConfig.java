package tecnico.kkn.spok;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.utils.TOTP;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.util.List;

/**
 * This class store a knocking configuration
 */
public class KnockConfig {
    private final static Logger log = LoggerFactory.getLogger(KnockConfig.class);

    private List<Short> sequence;
    private String name;
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<Short> getSequence() {
        return sequence;
    }

    public void setSequence(List<Short> sequence) {
        this.sequence = sequence;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void execute(String destination, long interval) throws InterruptedException, IOException {
        for(Short port: sequence) {
            knockPort(destination, port);
            Thread.sleep(interval * 1000);
        }

        // Decode the key into byte
        Base32 base32 = new Base32();
        byte[] byteKey = base32.decode(key);

        // Encode the key into hex
        // because our TOTP function use hex-encoded string as input
        String hexKey = Hex.encodeHexString(byteKey).toUpperCase();

        // Generate the TOTP code
        long T = (System.currentTimeMillis() / 1000) / 30;
        String steps = Long.toHexString(T).toUpperCase();
        while (steps.length() < 16) steps = "0" + steps;
        String totp = TOTP.generateTOTP(hexKey, steps, "6");
        log.debug("TOTP value: {}", totp);

        int totpPort = Integer.valueOf(totp) % 65536;
        log.debug("Port to knock: {}", totpPort);
        knockPort(destination, totpPort);
    }

    private void knockPort(String destination, int port) throws IOException {
        try {
            log.debug("Knocking {}:{}", destination, port);
            new Socket(destination, port);
        } catch (ConnectException e) {
            // The port is closed, so it's normal to have a ConnectException
        }
    }

    @Override
    public String toString() {
        return "KnockConfig{" +
                "sequence=" + sequence +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KnockConfig that = (KnockConfig) o;

        if (!name.equals(that.name)) return false;
        if (!sequence.equals(that.sequence)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sequence.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
