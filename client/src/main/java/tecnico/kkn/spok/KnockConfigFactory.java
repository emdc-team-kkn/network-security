package tecnico.kkn.spok;

import org.ini4j.Ini;
import org.ini4j.Profile;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * This class provide utilities to read configuration file and generate KnockConfig
 */
public class KnockConfigFactory {

    /**
     * Read the configuration file and return a set of KnockConfig
     * @param configFile
     * @return
     * @throws java.io.IOException
     */
    public Map<String, KnockConfig> readKnockConfigs(String configFile) throws IOException {
        Map<String, KnockConfig> knockConfigs = new HashMap<>();
        Ini iniFile = new Ini(new FileInputStream(configFile));

        for(String name: iniFile.keySet()) {
            Profile.Section section = iniFile.get(name);
            KnockConfig config = new KnockConfig();
            config.setName(name);
            List<Short> configSequence = new ArrayList<>();
            String[] sequence = section.get("sequence").split(",");
            for(String port: sequence) {
                configSequence.add(Short.valueOf(port));
            }
            config.setSequence(configSequence);
            if (section.containsKey("key")) {
                config.setKey(section.get("key"));
            }
            knockConfigs.put(name.toLowerCase(), config);
        }
        return knockConfigs;
    }
}
