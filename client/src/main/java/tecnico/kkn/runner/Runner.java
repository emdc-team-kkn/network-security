package tecnico.kkn.runner;

import org.kohsuke.args4j.*;
import org.kohsuke.args4j.spi.BooleanOptionHandler;
import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PcapNativeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.exception.KeyTooSmallException;
import tecnico.kkn.spok.KnockConfig;
import tecnico.kkn.spok.KnockConfigFactory;
import tecnico.kkn.utils.KeyTool;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * This is the runner class for the client application.
 * It contains the main method.
 */
public class Runner {
    private final Logger log = LoggerFactory.getLogger(Runner.class);

    @Option(name = "-c",
            usage = "The configuration file",
            metaVar = "<config file path>",
            depends = {"-k", "-d"},
            forbids = {"-g"})
    private String configurationFile;

    @Option(name = "-k",
            usage = "The name of the knock configuration to perform",
            metaVar = "<name of the knock configuration>")
    private String configName;

    @Option(name = "-d",
            usage = "The destination host address to knock",
            metaVar = "<host address of destination>")
    private String destination;

    @Option(name = "-i",
            usage = "The interval between the knock. Default: 1s",
            metaVar = "<interval in second>")
    private long interval = 1;

    @Option(name = "-s",
            usage = "The size of the key in bytes to be generated. Default: 20 bytes",
            metaVar = "<the length of the key>")
    private int keysize = 20;

    @Option(name = "-g",
            usage = "Generate the key to use in OTP",
            handler = BooleanOptionHandler.class,
            forbids = {"-c"})
    private boolean keygen = false;

    @Argument
    private List<String> arguments;

    public void run(String[] args) throws PcapNativeException, NotOpenException {
        ParserProperties parserProperties = ParserProperties.defaults();
        parserProperties.withUsageWidth(80);
        CmdLineParser parser = new CmdLineParser(this, parserProperties);

        try {
            parser.parseArgument(args);

            if(configurationFile == null && !keygen) {
                throw new CmdLineException(parser, "Either option -c or -g is required", null);
            }

            if (keygen) {
                genKey();
            } else {
                knock();
            }
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java tecnico.kkn.runner.Runner [options...] arguments...");
            parser.printUsage(System.err);
            parser.printExample(OptionHandlerFilter.ALL);
            System.err.println();
            System.exit(-1);
        } catch (IOException e) {
            log.debug("Unable to read configuration file", e);
            System.exit(-1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeyTooSmallException e) {
            System.err.println("Your key size is too small.");
            System.err.println("The minimum key size is 20 bytes");
        }
    }

    public void genKey() throws KeyTooSmallException {
        KeyTool keyTool = new KeyTool();
        String key = keyTool.genKey(keysize);
        System.out.println("Your key are generated in base32 encoding:");
        System.out.println(key);
    }

    public void knock() throws IOException, InterruptedException {
        log.debug("Loading configuration file: {}", configurationFile);
        KnockConfigFactory knockConfigFactory = new KnockConfigFactory();
        Map<String, KnockConfig> knockConfigs = knockConfigFactory.readKnockConfigs(configurationFile);
        log.debug("Knock configs: {}", knockConfigs);

        configName = configName.toLowerCase();
        if (!knockConfigs.containsKey(configName)) {
            System.err.println("Knocks configuration not found");
            System.exit(-1);
        } else {
            KnockConfig config = knockConfigs.get(configName);
            log.debug("Executing knock config: {}", config);
            log.debug("Destination address: {}", destination);
            config.execute(destination, interval);
        }
    }

    public static void main(String[] args) throws PcapNativeException, NotOpenException {
        new Runner().run(args);
    }

}
