package tecnico.kkn.utils;

import org.apache.commons.codec.binary.Base32;
import tecnico.kkn.exception.KeyTooSmallException;

import java.security.SecureRandom;
import java.util.Random;

/**
 * This class handle key generation & manipulation
 */
public class KeyTool {

    private final static int MIN_KEY_SIZE = 20;
    private final Random random = new SecureRandom();

    public String genKey(int keysize) throws KeyTooSmallException {
        if (keysize < MIN_KEY_SIZE) {
            throw new KeyTooSmallException();
        }

        // Generate a random key
        byte[] out = new byte[keysize];
        random.nextBytes(out);

        Base32 base32 = new Base32();
        return base32.encodeAsString(out);
    }

}
