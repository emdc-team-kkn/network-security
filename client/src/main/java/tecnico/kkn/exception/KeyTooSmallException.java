package tecnico.kkn.exception;

/**
 * This exception is used when the key size supplied to the keygen is too small.
 */
public class KeyTooSmallException extends Exception {
}
